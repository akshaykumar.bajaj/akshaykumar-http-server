const fs = require('fs');
const http = require('http');
const { v4: uuidv4 } = require('uuid');
const path = require('path');

function readFile(path) {
    return new Promise((resolve, reject) => {
        if (typeof path != 'string') {
            reject(new Error('Path should be a string'));
        }
        else {
            fs.readFile(path, 'utf-8', (error, data) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(data);
                }
            })
        }
    })
}
const server = http.createServer((req, res) => {
    if (req.method === 'GET') {
        if (path.basename(req.url) == 'html') {

            readFile('./index.html')
                .then((htmlContent) => {
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.write(htmlContent);
                    res.end();
                })
                .catch((error) => {
                    console.log("Error :" + error);
                    if (error.code === 'ENOENT') {
                        res.writeHead(404)
                        res.write('File Not Found');
                    }
                    else {
                        res.writeHead(500);
                        res.write('Eror');
                    }
                    res.end();
                })
            /* fs.readFile('./index.html', 'utf-8', (err, htmlContent) => {
                if (err) {
                    if (err.code === 'ENOENT') {
                        res.writeHead(404)
                        res.write(err.path + 'File Not Found');
                    }
                    else {
                        res.writeHead(500);
                        res.write('Eror:' + err);
                    }
                } else {
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.write(htmlContent);
                }
                res.end();
            }) */
        }
        else if (path.basename(req.url) == 'json') {
            readFile('./input.json')
                .then((jsonInput) => {
                    res.writeHead(200, { 'Content-Type': 'application/json' });
                    res.write(jsonInput);
                    res.end();
                })
                .catch((error) => {
                    console.log(error);
                    if (error.code === 'ENOENT') {
                        res.writeHead(404);
                        res.write("File Not Found");
                        res.end();
                    }
                    else {
                        res.writeHead(500);
                        res.write("An Error Occured!");
                        res.end();
                    }
                })
            /* fs.readFile('./input.json', 'utf-8', (err, jsonInput) => {
                if (err) {
                    if (err.code === 'ENOENT') {
                        res.writeHead(404)
                        res.write(err.path + ' File Not Found');
                    }
                    else {
                        res.writeHead(500);
                        res.write('Eror:' + err);
                    }
                } else {
                    res.writeHead(200, { 'Content-Type': 'application/json' });
                    res.write(jsonInput);
                }
                res.end();
            }); */
        }
        else if (path.basename(req.url) == 'uuid') {
            let uuid = { 'uuid': uuidv4() };

            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.write(JSON.stringify(uuid));
            res.end();
        }
        else if (req.url.includes('/status-code/')) {
            let statusCode = path.basename(req.url);
            let statusResponse = http.STATUS_CODES[statusCode];

            if (statusCode === 'status-code') {
                res.writeHead(422, 'Status Code not given');
                res.write('Status Code not given');
                res.end();

            } else if (statusResponse) {
                res.writeHead(parseInt(statusCode));
                res.write("Status Code:" + statusCode + "\n Status Response:" + statusResponse);
                res.end();
            }
            else {
                res.writeHead(404, "Response Not Found");
                res.write("Response Not found for Code" + statusCode);
                res.end();
            }
        }
        else if (req.url.includes('/delay/')) {
            let delayInSeconds = Number(path.basename(req.url));

            if (isNaN(delayInSeconds)) {
                res.writeHead(422, 'Delay time not a Number');
                res.write('Delay time should be a number');
                res.end();
            } else {
                let delayInMiliSeconds = delayInSeconds * 1000;

                res.setTimeout(delayInMiliSeconds, () => {
                    res.writeHead(200);
                    res.write(`Response given after ${delayInSeconds} seconds`);
                    res.end();
                })
            }
        }
        else {
            res.writeHead(404, "Page Not Found");
            res.write(req.url + "\n Page Not Found");
            res.end();
        }
    }
})

server.listen(80);